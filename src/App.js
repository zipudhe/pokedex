import React from 'react';
import Login from './Pages/Login';
import Home from './Pages/Home/';
import Profile from './Pages/Profile';
import Search from './Pages/Browse/pesquisa';
import { Router } from '@reach/router';

import './App.css';

const App = () => {

  return (
    <Router>
      <Login path='/'/>
      <Home path='/home/:name' />
      <Profile path='profile/:name' />
      <Search path = '/search/:name/:pokemon' />
    </Router>
  )
}
export default App;
