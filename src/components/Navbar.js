import React from 'react';
import { Link, navigate } from '@reach/router';
import { FaCircle } from 'react-icons/fa';
import './Navbar.css';

const Nav = ( obj ) => {

    const name = obj.profile

    const goTo = () => {
        navigate(`/home/${name}`);
    }

    return(
        <nav className="header">
            <div className="profile">
               <button onClick={goTo}> <FaCircle className="blue"/> </button>   <p>Bem vindo Treinador! </p>
            </div>
            <div className="menu">
                <Link to={`/home/${name}`} > <FaCircle className="red"/> <span> Pagina inicial </span> </Link>
                <Link to={`/profile/${name}`} perfil={obj} > <FaCircle className="green"/> Perfil </Link>
                <Link to={`/search/${name}/pikachu`} > <FaCircle className="yellow"/> Browse </Link>
            </div>    
        </nav>
    );

}


export default Nav;