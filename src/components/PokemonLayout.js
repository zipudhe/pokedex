import React from 'react';
import './PokemonLayout.css';
//import { Tipo } from './TipoPokemon';

const PokemonLayout = (objPokemon) => {
    const Pokemon = objPokemon;
    return(
        <div className="pokemon__card">
            <div className="pokemon--name">
                <p>{Pokemon.name}</p>
            </div>
            <div className="pokemon--img">
                <img src={Pokemon.img_url} />
            </div>
            <div className="pokemon--type">
                {/* <Tipo poke={Pokemon}/> */}
            </div>
        </div>
    );
}





export default PokemonLayout;