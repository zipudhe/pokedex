import React from "react"
import axios from "axios"
import { Tipo, Maiuscula, CorCard } from "./TipoPokemon"
//import { FaRegHeart, FaHeart } from 'react-icons/fa'
import './../Pages/Browse/pesquisa.css';
import { Link } from '@reach/router';

export function PokeHome (user,pkmn) {
    return (
        <Link to={`/search/${user}/${pkmn.name}`} >
        <div key={pkmn.id} className="pkmnCard" style={{ background: CorCard(pkmn.kind) }}>
            #{pkmn.number}: {Maiuscula(pkmn.name)} <br />
            {Tipo(pkmn)} <br />
            <img src={pkmn.image_url} alt={pkmn.name} />
        </div>
        </Link>
    )
}

function List(objProfile) {
    const user = objProfile.profile.username;
    const [page, setPage] = React.useState(1)
    const [lista, setLista] = React.useState([])


    React.useEffect(() => {
        if (page < 1) setPage(page => 1)
        else if (page > 33) setPage(page => 33)

        axios.get(`https://pokedex20201.herokuapp.com/pokemons/?page=${page}`)
            .then(retorno => setLista(retorno.data.data))
            .catch((err) => console.error(err))
    }, [page])

    function Scroll(num) {
        setPage(page => page + num)
    }

    function PokeHome (user,pkmn) {
        return (
            <Link to={`/search/${user}/${pkmn.name}`} >
            <div key={pkmn.id} className="pkmnCard" style={{ background: CorCard(pkmn.kind) }}>
                #{pkmn.number}: {Maiuscula(pkmn.name)} <br />
                {Tipo(pkmn)} <br />
                <img src={pkmn.image_url} alt={pkmn.name} />
            </div>
            </Link>
        )
    }

    return (
        <>
            <div className="buttons">
                <button onClick={ev => Scroll(-5)}>-5 Páginas</button>
                <button onClick={ev => Scroll(-1)}>Pg anterior</button>
                <button onClick={ev => Scroll(1)}>Prox pg</button>
                <button onClick={ev => Scroll(5)}>+5 Páginas</button>
            </div>
            <div className="pkmnHome">
                {lista ? lista.map(array => PokeHome(user ,array)) : ''}
            </div>
        </>
    )
}

export default List