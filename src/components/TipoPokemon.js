import React from 'react'

export function Tipo (poke) {
  function CorTipo (strpkmn, flag) {
    return <span className={[`kind${strpkmn}`, 'pkmnkind'].join(' ')} key={flag ? 0 : poke.id}>{Maiuscula(strpkmn)}</span>
  }

  let strpkmn = poke.kind
  strpkmn = strpkmn.split(';')
  
  let retorno = []
  retorno[0] = CorTipo(strpkmn[0], 0)
  retorno[1] = strpkmn[1] ? CorTipo(strpkmn[1], 1) : null
  
  return retorno
}

export function Maiuscula (strpkmn) {
  return strpkmn.charAt(0).toUpperCase() + strpkmn.slice(1);
}

function Cores (kind) {
  switch (kind) {
    case 'fighting':
      return '#6e3633'
    case 'steel':
      return '#B8B8D0'
    case 'electric':
      return '#af9738'
    case 'fire':
      return '#e99e69'
    case 'water':
      return '#445E9C'
    case 'grass':
      return '#a6c398'
    case 'flying':
      return '#878197'
    case 'poison':
      return '#7e5c7e'
    case 'dragon':
      return '#320c8d'
    case 'normal':
      return '#92927f'
    case 'rock':
      return '#b3a671'
    case 'ground':
      return '#6b6147'
    case 'psychic':
      return '#975569'
    case 'ghost':
      return '#2b203f'
    case 'dark':
      return '#220e02'
    case 'bug':
      return '#747943'
    case 'fairy':
      return '#e9b8c2'
    case 'ice':
      return '#77a7a7'
    default:
      return '#ffffff'
  }

}

export function CorCard (kind) {
  if (kind == null) return
  kind = kind.split(';')
  
  if (kind[1] === undefined) return Cores(kind[0])

  return `linear-gradient(to right, ${Cores(kind[1])} , ${Cores(kind[0])})`
}