import api from './api';

// Não precisa receber parâmetro e retorna a lista de todos os pokemons
export const getPokemonsList = api.get('/pokemons');

// recebe como parâmetro o nome do pokemon que é retornado com todos os seus dados
export const getPokemon = (pokemonName) => api.get(`/pokemons/${pokemonName}`);

