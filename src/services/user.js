import api from './api';

// recebe um objeto contendo o nome do usuario e recebe o usuario
export const getUser = (data) => api.get(`/users/${data.name}`);

// recebe um objeto do usuario e cria um novo usuario 
export const postUser = (data) => api.post(`/users/`, data);

// recebe o usuario e pokemon e o acrescenta à lista de favortios do usuario
export const postStarredPokemon = (profile, pokemon) => api.post(`/users/${profile.username}/starred/${pokemon}`);

// recebe o usuario e pokemon o  e tira da lista de favortios
export const deleteStarredPokemon = (profile, pokemon) => api.delete(`/users/${profile.username}/starred/${pokemon}`);
