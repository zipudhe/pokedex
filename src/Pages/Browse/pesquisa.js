import React from 'react';
import axios from 'axios';
import { Tipo, Maiuscula } from '../../components/TipoPokemon'
import logo from '../../assets/missingno.png'
import './pesquisa.css'
import Nav from '../../components/Navbar';
import { FaRegHeart, FaHeart } from 'react-icons/fa'

function tamanho (likeslist) {
  let i = 0;
  while (likeslist[i]) {
    i++
  }
  return i
}

function Search({name, pokemon}) {
  const [inp, setInp] = React.useState();
  const [sel, setSel] = React.useState(pokemon);
  const [poke, setPoke] = React.useState();
  const [likes, setLikes] = React.useState()
  const [flag, setFlag] = React.useState(false)
  const [heart, setHeart] = React.useState()
    
  function UnLike (pkmn, name) {
    axios.delete(`https://pokedex20201.herokuapp.com/users/${name}/starred/${pkmn.name}/`)
      .then(setFlag(flag => !flag))
      .catch((err) => console.error(err))
  }

  function Like (pkmn, name) {
    axios.post(`https://pokedex20201.herokuapp.com/users/${name}/starred/${pkmn.name}/`)
      .then(setFlag(flag => !flag))
      .catch((err) => console.error(err))
  }

  function Send(event) {
    event.preventDefault()
    setSel(sel => inp.toLowerCase())
    setInp(inp => '')
  }


  React.useEffect(() => {
    if (!poke || !likes) {
      setHeart(<FaRegHeart  className="nolike" onClick={ev => Like(poke, name)}/>)
    }
    else {
      setHeart(<FaRegHeart  className="nolike" onClick={ev => Like(poke, name)}/>)
      
      for (let i = 0; i < tamanho(likes); i++ ){
        if (poke.name === likes[i].name) {
          setHeart(<FaHeart  className="liked" onClick={ev => UnLike(poke, name)}/>)
        }
      }
    }
  }, [poke, flag, likes, name])

  React.useEffect(() => {
    axios.get("https://pokedex20201.herokuapp.com/pokemons/" + sel)
      .then(retorno => setPoke(retorno.data))
      .catch((err) => console.error(err))
  }, [sel])

  React.useEffect(() => {
    axios.get(`https://pokedex20201.herokuapp.com/users/${name}`)
      .then(retorno => setLikes(retorno.data.pokemons))
      .catch((err) => console.error(err))
  }, [poke, name, flag, likes])

  return (
    <>
      <Nav profile={name}/>
      <div className="main__content">
        <form onSubmit={Send} className="inputBrowse" >
          <input
            type="text"
            value={inp}
            onChange={ev => setInp(ev.target.value)}
            placeholder='Pokemon' 
          />
        </form>

        <div className="pgPkmn">
          <div className="box-starred">
            <div className="heart">
              {heart}
            </div>
            <div className="pkdxNumber">
             <span> #{poke ? poke.number : '0'} </span>
            </div>
          </div>
            <div className="box">

            <div className="status">
             <p> Nome: {poke ? Maiuscula(poke.name) : 'Missingno'} </p>
             <p> Altura: {poke ? (poke.height/10) + 'm' : '-2048'}</p>
             <p> Peso: {poke ? (poke.weight/10) + 'kg' : '124Mb'} </p>
             <p> Tipo: {poke ? Tipo(poke).map(array => array) : 'Error 404 Pokemon not found'} </p>
            </div>
            <div className="img">
              <img src={poke ? poke.image_url : logo} alt={poke ? poke.name : 'missigno'} width="200px" />
            </div>

          </div>

        </div>
      </div>
    </>
  )
}

export default Search;
