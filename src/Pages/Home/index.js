import React from 'react';
import './Home.css';
import Nav from '../../components/Navbar';
import List from '../../components/ListPokemons';
import { getUser } from '../../services/user';

const Home = (objProfile) => {
    const [user, setUser] = React.useState([]);
    const [starred, setStarred] = React.useState([]);

    console.log(starred)
    React.useEffect(() => {
        getUser(objProfile)
            .then(res => {
                setUser(res.data.user);
                setStarred(res.data.pokemons);
            })
            .catch(err => console.error(err.response));
    }, [objProfile]);

    return (
        <>
            <Nav profile={user.username} />
            <div className="main__content">
                <List profile={user} />
            </div>
        </>
    )
}

export default Home;