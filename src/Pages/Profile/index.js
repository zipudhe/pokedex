import React from 'react';
import './Perfil.css';
import Nav from '../../components/Navbar';
import { FaCircle } from 'react-icons/fa'
import { Link } from '@reach/router'
import { PokeHome } from './../../components/ListPokemons'

import img from '../../assets/loginIcon.png';
import { getUser } from '../../services/user';

const Profile = (obj) => {
    const [starred, setStarred] = React.useState([]);
    const [user, setUser] = React.useState([]);
    React.useEffect(() => {
        getUser(obj)
            .then(res => {
                setStarred(res.data.pokemons);
                setUser(res.data.user);
            }
            )
            .catch(err => console.error(err));
    }, [obj]);

    return (
        <>
            <Nav profile={user.username} />
            <div className="wrap">
                <div className="sidebar">
                    <div className="profile__trainer">
                        <div className="trainer--data">
                            <p>{user.username}</p>
                        </div>
                        <img src={img} alt="profile" />
                        <p>{`#ID: ${user.id}`}</p>
                    </div>
                    <div className="aesthetic">
                        <FaCircle className="red" />
                        <FaCircle className="green" />
                        <FaCircle className="yellow" />
                    </div>
                    <div className="exit">
                        <button>  <Link to="/"> SAIR </Link> </button>
                    </div>
                </div>
                <div className="starred">
                    {starred.map(element => PokeHome(user.username, element))}
                </div>
            </div>
        </>
    )
}

export default Profile;