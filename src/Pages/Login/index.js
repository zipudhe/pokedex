import React from 'react';
import { getUser, postUser } from '../../services/user';
import './Login.css'
import { navigate } from '@reach/router';
import logo from '../../assets/loginIcon.png'

const Login = () => {
    const [user, setUSer] = React.useState('');


    const userSettler = (e) => {
        let user = e.target.value;
        if (user !== '') {
            setUSer(user);
        }
    }

    const profileData = {
    }

    const createUser = profileData => {
        postUser(profileData)
            .then(res => {
                console.log(res.data.username);
                navigate(`/home/${res.data.username}`);
            })
            .catch(err => {
                alert(err.response.statusText);
            })
    }



    const handleUser = (e) => {
        let name = 'name';
        e.preventDefault();
        profileData[e.target.elements.username.name] = user;
        profileData[name] = user;
        console.log(profileData)
        getUser(profileData)
            .then(res => {
                if (res.status === 200) navigate(`/home/${profileData.username}`);
            })
            .catch(err => {
                alert('Não tinha uma conta? Agr tem!! Seja Bem vindo!')
                createUser(profileData)
            });
    }

    return (
        <div className="background">
            <div className="form">
                <div className="login">
                    <img
                        src={logo}
                        alt="login"
                    />
                </div>
                <div className="input-form">
                    <form action="#" onSubmit={handleUser} >
                        <input
                            type="text"
                            name="username"
                            placeholder="Username"
                            onChange={userSettler} />
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Login;